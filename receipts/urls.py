from django.urls import path
from receipts.views import list_receipts, create_receipt, list_expense_category, list_accounts, create_expense_category, create_account


urlpatterns = [
    path("", list_receipts, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", list_expense_category, name="list_expense_category"),
    path("categories/create/", create_expense_category, name="create_category"),
    path("accounts/", list_accounts, name="list_accounts"),
    path("accounts/create/", create_account, name="create_account"),

]
